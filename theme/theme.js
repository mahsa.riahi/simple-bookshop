import { createTheme} from '@mui/material/styles';

export const Theme = createTheme({
    pallete:{
        primary:{
            main: "#6683d2",
            light:"#5b6cbf",
            dark:"#1831a4"
        },
        text:{
            primary: "#1831a4",
            secondary: "#000000"
        },
    },
    bShadow: {
        default :"0 0 10px 2px rgb(180, 180, 180)",
        hover: "0 0 10px 2px #5b6cbf "
    },
    mr:[
            "calc(50% - 60px)",
            "calc(50% - 90px)"
        ],

})



