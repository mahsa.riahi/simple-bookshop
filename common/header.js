import * as React from 'react';
import Link from "next/link"
import { useSelector } from "react-redux";
import { isEmpty } from "lodash";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';



const Header = () =>{

    const user = useSelector(state => state)
   
    return(

    <Box component="header"
              sx={{ flexGrow: 1 }}
    >
      <AppBar position="static"
                     sx={{width: 1 ,
                                 height :70,
                                 bgcolor: (theme) => theme.pallete.primary.main
                            }}
      >
        <Toolbar>
          <Link  href="/book" passHref>
            <Button color="inherit"
                          variant="text"
                          sx={{fontSize:"20px",
                                  border:1,
                                  borderColor:"white",
                                  m: "10%"
                                  }}      
            >
              لیست کتاب ها
            </Button>
          </Link>
          {!isEmpty(user) ?  <Box component="div"
                                                  sx={{position: "absolute",
                                                          left:"10%"
                                                        }}
                                        >
                                          <Link href="/dashboard" passHref>
                                            <Button color="inherit" 
                                                          variant="text"
                                                          sx={{fontSize:"18px",
                                                                    border:1,
                                                                    borderColor:"white"
                                                                    }}
                                            >
                                              {user.name}
                                            </Button>
                                          </Link>
                                          <Link  href="/logout" passHref >
                                            <Button color="inherit" 
                                                          variant="text"
                                                          sx={{mr : 2,
                                                                   fontSize:"20px",
                                                                    border: 1,
                                                                    borderColor:"white"
                                                                    }}
                                            >
                                              خروج
                                            </Button>
                                          </Link>
                                        </Box>
                                      : <Link href="/login" passHref>
                                          <Button color="inherit" 
                                                        variant="text"
                                                        sx={{position : "absolute" , 
                                                                left: "10%",
                                                                fontSize:"20px",
                                                                border:1,
                                                                borderColor:"white"
                                                                }}
                                          >
                                            ورود
                                          </Button>
                                        </Link>
    }
        </Toolbar>
      </AppBar>
    </Box>
  )
}

export default Header