import "@/css/style.css"
import MainLayout from "component/mainLayout"
import { Provider } from "react-redux";
import {store} from "@/store/store"
import { ThemeProvider} from '@mui/material/styles';
import {Theme} from "theme/theme"

 const App = ({Component , pageProps}) => {
     
    return (
        <ThemeProvider theme={Theme}>
            <Provider store={store}>
                    <MainLayout>
                        <Component {...pageProps} />
                    </MainLayout>    
             </Provider>
             </ThemeProvider>
             )
}

export default App

