import React , {Fragment} from "react";
import Head from "next/head"
import Link from "next/link"
import Typography from '@mui/material/Typography';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';
import { Container } from "@mui/material";
import Box from '@mui/material/Box';


const Books = ({books}) =>{

return(
<Fragment >
    <Head>
        <title>
            لیست کتاب ها
         </title>
    </Head>
    <Box  component="section"
               sx={{mt:10}
    }>
        <Container fixed
                            sx={{m:"auto",
                                    mb:10
                                   }}  
        > 
                <Grid container
                         rowSpacing={3.5}
                         columnSpacing={4}      
                >
                {books.map(
                 book =>  
                    <Grid item
                              key={book.slug}
                              component="div"
                              xs={12}
                              sm={6}
                              md={4}
                              lg={3}
                              xl={3}
                    >
                        <Link  href={`/book/${book.slug}`}
                                    passHref
                        >
                            <Card sx={{ pt:4,
                                                cursor:"pointer",
                                                transition:"0.5s",
                                                ':hover': {
                                                     boxShadow: (theme) => theme.bShadow.hover }
                                              }}
                            >
                                <CardMedia component="img"
                                                      image={book.cover}
                                                      alt={book.titleFa}
                                                      sx={{boxShadow: (theme) => theme.bShadow.default,
                                                              width:200,
                                                              m:"auto"
                                                            }}
                                />
                                <CardContent sx={{ mt:2,
                                                                  width:1
                                                               }}
                                >
                                     <Typography variant="h6"
                                                            component="h1"
                                                            sx={{ fontWeight:"bolder",
                                                                     fontSize:"18px",
                                                                     color: (theme) => theme.pallete.text.secondary
                                                                   }}
                                    >
                                        {book.titleFa}
                                    </Typography>
                                     <Typography variant="h5"
                                                            component="h1"
                                                            sx={{ fontSize:"15px",
                                                                     direction:"ltr",
                                                                     mt:2,
                                                                     fontWeight:"bolder" ,
                                                                     width:1,
                                                                     mr: -4,
                                                                     color: (theme) => theme.pallete.text.secondary
                                                                  }}
                                    >
                                        {book.titleEn}
                                    </Typography>
                                    <Typography variant="h5"
                                                            component="h1"
                                                            sx={{ mt:2,
                                                                     fontSize:"18px",
                                                                     color: (theme) => theme.pallete.text.secondary
                                                                    }}
                                            >
                                         {book.author.nameFa}
                                    </Typography>
                                 </CardContent>
                            </Card>
                         </Link>
                    </Grid>
                    )}
              
                </Grid>
            </Container> 
        </Box>
    </Fragment>

        )
    }

export async function getStaticProps() {
    const res = await fetch("https://api.staging.bookapo.com/book/feed/last")
    const data = await res.json()
  
    return{
        props:{
           books : data.data.results
        }
    }

}

export default Books