import Link from "next/link"
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography'
import { Button } from "@mui/material";


const Invalidation = () =>{


return(
    <Box
        component="section"
        sx={{
            display: 'flex',
            '& > :not(style)': {
            m: 1,
            width: 410,
            height: 260,
            m:"auto",
            mt:20
        },
        }}
    >
        <Paper color="background"
                    sx={{ p:2,
                             boxShadow:"none"
                        }}
        >
            <Typography  variant="h4" 
                                    component="h1"
                                    sx={{ fontWeight:"bolder",
                                             mt:4,
                                             textAlign:"center",
                                             color: (theme) => theme.pallete.text.secondary,
                                           }}
             >
                 ابتدا وارد حساب کاربری شوید
            </Typography>
            <Link href="/" passHref>
                <Button variant="contained"
                              size="large"
                              sx={{width:120,
                                      height:50,
                                      mr: (theme) => theme.mr[0],
                                      mt: 6,
                                      fontWeight:"bolder",
                                      fontSize:"17px",
                                      bgcolor: (theme) => theme.pallete.primary.main,
                                      ":hover":{
                                        bgcolor: (theme) => theme.pallete.primary.dark
                                      },
                                    }}
                >
                    صفحه اصلی
                </Button>
            </Link>
        </Paper>
    </Box>
        
    )
}
export default Invalidation