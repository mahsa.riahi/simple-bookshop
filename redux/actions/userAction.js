
export const userLogin = (user) =>{
    return async (dispatch) =>{
        await dispatch({type : "USER-LOGIN" , payload : user })
    }

}

export const userLogout = () =>{
    return async (dispatch) =>{
        await dispatch({type : "USER-LOGOUT" , payload : {}})
    }


}